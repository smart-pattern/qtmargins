import requests
secret_token = "<your secret token>"
project_id = "17414226"
file_path = "your_file.txt"
api_root = "https://gitlab.com/api/v4"

headers = {
    'PRIVATE-TOKEN': secret_token,
}

uri = '{}/projects/{}/uploads'.format(api_root, project_id)

files = {
    'file': open(file_path, 'rb')
}


r_upload = requests.post(uri, headers=headers, files=files)
if r_upload.status_code != 201 and r_upload.status_code != 200:
    raise ValueError("Upload API responded with invalid status {}".format(r_upload.status_code))  # noqa: E501

upload = r_upload.json()

print(upload)
