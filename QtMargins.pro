include(common.pri)

QT       += core gui widgets printsupport

# We want create executable file
TEMPLATE = app

# Name of binary file
macx{
    TARGET = QtMargins
} else {
    TARGET = qtmargins
}

#Check if Qt version >= 5.0.0
!minQtVersion(5, 0, 0) {
    message("Cannot build QtMargins with Qt version $${QT_VERSION}.")
    error("Use at least Qt 5.0.0.")
}

# Use out-of-source builds (shadow builds)
CONFIG -= debug_and_release debug_and_release_target

# Since Q5.12 available support for C++17
equals(QT_MAJOR_VERSION, 5):greaterThan(QT_MINOR_VERSION, 11) {
    CONFIG += c++17
} else {
    CONFIG += c++14
}

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

# Since Qt 5.4.0 the source code location is recorded only in debug builds.
# We need this information also in release builds. For this need define QT_MESSAGELOGCONTEXT.
DEFINES += QT_MESSAGELOGCONTEXT

# Directory for executable file
DESTDIR = bin

# Directory for files created moc
MOC_DIR = moc

# Directory for objecs files
OBJECTS_DIR = obj

# Directory for files created rcc
RCC_DIR = rcc

# Directory for files created uic
UI_DIR = uic

# Suport subdirectories. Just better project code tree.
include(src/QtMargins.pri)

CONFIG(release, debug|release){
    # Release mode
    !*msvc*:CONFIG += silent
} else {
# Breakpoints do not work if debug the app inside of bundle. In debug mode we turn off creating a bundle.
# Probably it will breake some dependencies. Version for Mac designed to work inside an app bundle.
    CONFIG -= app_bundle
}

# Some extra information about Qt. Can be usefull.
message(Qt version: $$[QT_VERSION])
message(Qt is installed in $$[QT_INSTALL_PREFIX])
message(Qt resources can be found in the following locations:)
message(Documentation: $$[QT_INSTALL_DOCS])
message(Header files: $$[QT_INSTALL_HEADERS])
message(Libraries: $$[QT_INSTALL_LIBS])
message(Binary files (executables): $$[QT_INSTALL_BINS])
message(Plugins: $$[QT_INSTALL_PLUGINS])
message(Data files: $$[QT_INSTALL_DATA])
message(Translation files: $$[QT_INSTALL_TRANSLATIONS])
message(Settings: $$[QT_INSTALL_SETTINGS])
message(Examples: $$[QT_INSTALL_EXAMPLES])

# Path to recource file.
win32:RC_FILE = dist/win/qtmargins.rc

# Set "make install" command for Unix-like systems.
unix{
    # Prefix for binary file.
    isEmpty(PREFIX){
        PREFIX = $$DEFAULT_PREFIX
    }

    unix:!macx{
        DATADIR =$$PREFIX/share
        DEFINES += DATADIR=\\\"$$DATADIR\\\" PKGDATADIR=\\\"$$PKGDATADIR\\\"

        # Path to bin file after installation
        target.path = $$PREFIX/bin

        # .desktop file
        desktop.path = $$PREFIX/share/applications/
        desktop.files += dist/$${TARGET}.desktop

        # logo
        hicolor_48_apps.path = $$PREFIX/share/icons/hicolor/48x48/apps/
        hicolor_48_apps.files += \
            dist/share/icons/48x48/apps/$${TARGET}.png

        hicolor_64_apps.path = $$PREFIX/share/icons/hicolor/64x64/apps/
        hicolor_64_apps.files += \
            dist/share/icons/64x64/apps/$${TARGET}.png

        hicolor_128_apps.path = $$PREFIX/share/icons/hicolor/128x128/apps/
        hicolor_128_apps.files += \
            dist/share/icons/128x128/apps/$${TARGET}.png

        hicolor_256_apps.path = $$PREFIX/share/icons/hicolor/256x256/apps/
        hicolor_256_apps.files += \
            dist/share/icons/256x256/apps/$${TARGET}.png

        hicolor_512_apps.path = $$PREFIX/share/icons/hicolor/512x512/apps/
        hicolor_512_apps.files += \
            dist/share/icons/512x512/apps/$${TARGET}.png

        INSTALLS += \
            target \
            desktop \
            hicolor_48_apps \
            hicolor_64_apps \
            hicolor_128_apps \
            hicolor_256_apps \
            hicolor_512_apps
    }
    macx{
        # Some macx stuff
        QMAKE_MAC_SDK = macosx

        # QMAKE_MACOSX_DEPLOYMENT_TARGET defined in common.pri

        CONFIG(release, debug|release){
            QMAKE_RPATHDIR += @executable_path/../Frameworks

            # Path to resources in app bundle
            #RESOURCES_DIR = "Contents/Resources" defined in translation.pri
            FRAMEWORKS_DIR = "Contents/Frameworks"
            MACOS_DIR = "Contents/MacOS"
            # On macx we will use app bundle. Bundle doesn't need bin directory inside.
            # See issue #166: Creating OSX Homebrew (Mac OS X package manager) formula.
            target.path = $$MACOS_DIR

            #languages added inside translations.pri

            # logo on macx.
            ICON = dist/macx/QtMargins.icns

            QMAKE_INFO_PLIST = $$PWD/dist/macx/Info.plist
        }
    }
}

# "make install" command for Windows.
# Depend on inno setup script and create installer in folder "package"
win32:*g++* {
    package.path = $${OUT_PWD}/../../../package/qtmargins
    package.files += \
        $${OUT_PWD}/$${DESTDIR}/qtmargins.exe \
        $$PWD/../../../dist/win/qtmargins.ico \
        $$[QT_INSTALL_BINS]/Qt5Core.dll \
        $$[QT_INSTALL_BINS]/Qt5Gui.dll \
        $$[QT_INSTALL_BINS]/Qt5PrintSupport.dll \
        $$[QT_INSTALL_BINS]/Qt5Widgets.dll \
        $$[QT_INSTALL_BINS]/libgcc_s_*-1.dll \ # There are several different exception handler for MinGW available: sjlj, dwarf, seh
        $$[QT_INSTALL_BINS]/libstdc++-6.dll \
        $$[QT_INSTALL_BINS]/libwinpthread-1.dll

    package.CONFIG = no_check_exist
    INSTALLS += package

    package_bearer.path = $${OUT_PWD}/../../../package/qtmargins/bearer
    package_bearer.files += \
        $$[QT_INSTALL_PLUGINS]/bearer/qgenericbearer.dll \
        $$[QT_INSTALL_PLUGINS]/bearer/qnativewifibearer.dll
    INSTALLS += package_bearer

    package_imageformats.path = $${OUT_PWD}/../../../package/qtmargins/imageformats
    package_imageformats.files += \
        $$[QT_INSTALL_PLUGINS]/imageformats/qico.dll \
        $$[QT_INSTALL_PLUGINS]/imageformats/qjp2.dll \
        $$[QT_INSTALL_PLUGINS]/imageformats/qjpeg.dll \
    INSTALLS += package_imageformats

    package_platforms.path = $${OUT_PWD}/../../../package/qtmargins/platforms
    package_platforms.files += $$[QT_INSTALL_PLUGINS]/platforms/qwindows.dll
    INSTALLS += package_platforms

    package_printsupport.path = $${OUT_PWD}/../../../package/qtmargins/printsupport
    package_printsupport.files += $$[QT_INSTALL_PLUGINS]/printsupport/windowsprintersupport.dll
    INSTALLS += package_printsupport

    # Since 5.10, platform styles such as QWindowsVistaStyle, QMacStyle, etc., are no longer embedded in the QtWidgets
    # library.
    greaterThan(QT_MAJOR_VERSION, 4):greaterThan(QT_MINOR_VERSION, 9) {
        package_styles.path = $${OUT_PWD}/../../../package/qtmargins/styles
        package_styles.files += $$[QT_INSTALL_PLUGINS]/styles/qwindowsvistastyle.dll
        INSTALLS += package_styles
    }
}

CONFIG(release, debug|release){
    macx{
       # run macdeployqt to include all qt libraries in packet
       QMAKE_POST_LINK += $$[QT_INSTALL_BINS]/macdeployqt $${OUT_PWD}/$${DESTDIR}/$${TARGET}.app
    }
}

RESOURCES += \
    dist/icon.qrc \
    dist/share/theme.qrc
