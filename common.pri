defineTest(minQtVersion) {
    maj = $$1
    min = $$2
    patch = $$3
    isEqual(QT_MAJOR_VERSION, $$maj) {
        isEqual(QT_MINOR_VERSION, $$min) {
            isEqual(QT_PATCH_VERSION, $$patch) {
                return(true)
            }
            greaterThan(QT_PATCH_VERSION, $$patch) {
                return(true)
            }
        }
        greaterThan(QT_MINOR_VERSION, $$min) {
            return(true)
        }
    }
    greaterThan(QT_MAJOR_VERSION, $$maj) {
        return(true)
    }
    return(false)
}

macx{
    # Check which minimal OSX version supports current Qt version
    # See page https://doc.qt.io/qt-5/supported-platforms-and-configurations.html
    # For older versions https://doc.qt.io/qt-5.11/supported-platforms-and-configurations.html
    equals(QT_MAJOR_VERSION, 5):greaterThan(QT_MINOR_VERSION, 13) {# Qt 5.14
        QMAKE_MACOSX_DEPLOYMENT_TARGET = 10.13
    } else {
        equals(QT_MAJOR_VERSION, 5):greaterThan(QT_MINOR_VERSION, 11) {# Qt 5.12
            QMAKE_MACOSX_DEPLOYMENT_TARGET = 10.12
        } else {
            equals(QT_MAJOR_VERSION, 5):greaterThan(QT_MINOR_VERSION, 10) {# Qt 5.11
                QMAKE_MACOSX_DEPLOYMENT_TARGET = 10.11
            } else {
                equals(QT_MAJOR_VERSION, 5):greaterThan(QT_MINOR_VERSION, 9) {# Qt 5.10
                    QMAKE_MACOSX_DEPLOYMENT_TARGET = 10.11
                } else {
                    equals(QT_MAJOR_VERSION, 5):greaterThan(QT_MINOR_VERSION, 8) {# Qt 5.9
                        QMAKE_MACOSX_DEPLOYMENT_TARGET = 10.10
                    } else {
                        equals(QT_MAJOR_VERSION, 5):greaterThan(QT_MINOR_VERSION, 7) {# Qt 5.8
                            QMAKE_MACOSX_DEPLOYMENT_TARGET = 10.9
                        } else {
                            equals(QT_MAJOR_VERSION, 5):greaterThan(QT_MINOR_VERSION, 6) {# Qt 5.7
                                QMAKE_MACOSX_DEPLOYMENT_TARGET = 10.8
                            } else {
                                equals(QT_MAJOR_VERSION, 5):greaterThan(QT_MINOR_VERSION, 3) {# Qt 5.4
                                    QMAKE_MACOSX_DEPLOYMENT_TARGET = 10.7
                                } else {
                                    QMAKE_MACOSX_DEPLOYMENT_TARGET = 10.6
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

DEFINES += QT_NO_FOREACH
