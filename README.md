# QtMargins

![Example of interface](img/interface.png "Example of interface")

Simple utility to test printing with Qt libary.

The main purpose of this utility to test setting printer margins. We have found that in some cases Qt library can have issues with printer margins. This may be caused by a bug in Qt or printer driver. Use this utility to quickly find which value in which case work for you.

This project requires Qt 5.

Main features:

* Print pages with selected printer
* Print in PDF file
* Print with XPS printer
* Show print preview

## Authors

* **Roman Telezhynskyi**

## License

This project is licensed under the GPL v3 - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* https://bugreports.qt.io/browse/QTBUG-32987

