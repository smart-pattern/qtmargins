SOURCES += \
    $$PWD/global.cpp \
    $$PWD/main.cpp \
    $$PWD/mainwindow.cpp

HEADERS += \
    $$PWD/global.h \
    $$PWD/mainwindow.h \
    $$PWD/qoverload.h \
    $$PWD/version.h

FORMS += \
    $$PWD/mainwindow.ui
