/************************************************************************
 **
 **  @file   %{Cpp:License:FileName}
 **  @author Roman Telezhynskyi <dismine(at)gmail.com>
 **  @date   10 3, 2020
 **
 **  Valentina is free software: you can redistribute it and/or modify
 **  it under the terms of the GNU General Public License as published by
 **  the Free Software Foundation, either version 3 of the License, or
 **  (at your option) any later version.
 **
 **  Valentina is distributed in the hope that it will be useful,
 **  but WITHOUT ANY WARRANTY; without even the implied warranty of
 **  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 **  GNU General Public License for more details.
 **
 **  You should have received a copy of the GNU General Public License
 **  along with Valentina.  If not, see <http://www.gnu.org/licenses/>.
 **
 *************************************************************************/
#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QPrinter>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class QCheckBox;
class QComboBox;
class QGroupBox;
class QSpinBox;

typedef QSharedPointer<QPainter> PainterPtr;

enum MarginsAction
{
    MarginsUnchanged,
    MarginsCleared,
    MarginsDoubled,
    MarginsHalved,
    MarginsFixed20,
    MarginsFixed40,
    MarginsCustom
};

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    QPrinter::PrinterMode printerMode() const;
    void setPrinterMode(const QPrinter::PrinterMode &printerMode);

    int pageCount() const;
    void setPageCount(int pageCount);

private slots:
    void on_actionPrinter_triggered();
    void on_actionQuit_triggered();
    void on_actionPrintPDF_triggered();
    void on_actionPrintXPS_triggered();
    void on_actionPrintPreview_triggered();
    void on_actionReset_triggered();
    void on_actionAboutQt_triggered();
    void on_actionOpenPDFFolder_triggered();
    void on_actionAboutApp_triggered();
    void paintRequested(QPrinter *printer);

private:
    Ui::MainWindow *ui;
    QPrinter::PrinterMode m_printerMode{QPrinter::ScreenResolution};
    int m_pageCount{2};

    QVector<QGroupBox *> m_enabled{};
    QVector<QCheckBox *> m_fullPage{};
    QVector<QCheckBox *> m_use53MarginsApi{};
    QVector<QComboBox *> m_marginsAction{};

    QVector<QSpinBox *> m_leftMargin{};
    QVector<QSpinBox *> m_rightMargin{};
    QVector<QSpinBox *> m_topMargin{};
    QVector<QSpinBox *> m_bottomMargin{};

    QStringList m_pdfFiles{};

    void InitUI();
    void printPageWithFrame(QPrinter *printer, int page, bool fullPage, bool use53MarginsApi,
                            MarginsAction marginsAction, PainterPtr &painter);
#if QT_VERSION >= QT_VERSION_CHECK(5, 3, 0)
    QMarginsF CustomMargins(int page);
#endif

    void PrintToFile(QPrinter &printer, const QString &suffix);
};
#endif // MAINWINDOW_H
