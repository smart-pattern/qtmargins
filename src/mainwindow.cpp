/************************************************************************
 **
 **  @file   %{Cpp:License:FileName}
 **  @author Roman Telezhynskyi <dismine(at)gmail.com>
 **  @date   10 3, 2020
 **
 **  Valentina is free software: you can redistribute it and/or modify
 **  it under the terms of the GNU General Public License as published by
 **  the Free Software Foundation, either version 3 of the License, or
 **  (at your option) any later version.
 **
 **  Valentina is distributed in the hope that it will be useful,
 **  but WITHOUT ANY WARRANTY; without even the implied warranty of
 **  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 **  GNU General Public License for more details.
 **
 **  You should have received a copy of the GNU General Public License
 **  along with Valentina.  If not, see <http://www.gnu.org/licenses/>.
 **
 *************************************************************************/
#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QApplication>
#include <QDate>
#include <QDebug>
#include <QDir>
#include <QFile>
#include <QPainter>
#include <QPrintDialog>
#include <QPrintPreviewDialog>
#include <QPrinterInfo>
#include <QProcess>
#include <QDesktopWidget>
#include <QMessageBox>
#include <QDesktopServices>
#include <QUrl>

#include "qoverload.h"
#include "global.h"

namespace
{
const QStringList marginActionTexts(
{
    "Margins unchanged",
    "Clear Margins",
    "Double Margins",
    "Halve Margins",
    "Margins=20mm",
    "Margins=40mm",
    "Custom"
});

QDebug operator<<(QDebug d, const QPagedPaintDevice::Margins &m)
{
    d << "QPagedPaintDevice::Margins(" << m.left << ", "
                                       << m.top << ", "
                                       << m.right << ", "
                                       << m.bottom << ')';
    return d;
}

void dumpPageSize(QDebug &d, const QPrinter *printer)
{
    d << "\nQPrinter.pageRect     = " << printer->pageRect(QPrinter::Millimeter) << "mm"
      << "\n                      = " << printer->pageRect(QPrinter::DevicePixel) << " device pixel"
      << "\nQPrinter.paperRect    = " << printer->paperRect(QPrinter::Millimeter) << "mm"
      << "\n                      = " << printer->paperRect(QPrinter::DevicePixel) << " device pixel"
      << "\nQPrinter.fullPage     = " << printer->fullPage()
      << "\nQPagedPaintDevice.margins(struct) ="
         "\n   "  << printer->margins() << "mm";
    qreal left, top, right, bottom;
    printer->getPageMargins(&left, &top, &right, &bottom, QPrinter::Millimeter);
    d << "\nQPrinter.getPageMargins(4xqreal) = "  << left << ", " << top << ", " << right << ", " << bottom << "mm"
      << '\n';
    const QPageLayout l = printer->pageLayout();
    d << l
      << "\nQPageLayout.paintRect= " << l.paintRect(QPageLayout::Millimeter) << "mm"
      << "\nQPageLayout.fullRect = " << l.fullRect(QPageLayout::Millimeter) << "mm"
      << "\nQPageLayout.margins  = " << l.margins(QPageLayout::Millimeter) << "mm"
      << "\nQPageLayout.mode     = " << l.mode() << '\n';
}

void drawPageRect(QRectF r, const QPointF &offset,
                  QPainter &painter, bool dash = false)
{
    r.setBottomRight(r.bottomRight() - QPoint(1, 1));
    r.moveTopLeft(r.topLeft() - offset);
    r.setTopLeft(r.topLeft() + QPoint(1, 1));
    qDebug() << __FUNCTION__ << r << offset << dash;

    const QPen oldPen = painter.pen();
    if (dash)
    {
        QPen pen = oldPen;
        pen.setStyle(Qt::DashLine);
        painter.setPen(pen);
    }
    painter.drawRect(r);
    if (dash)
    {
        painter.setPen(oldPen);
    }
}

class NonModalPreviewDialog : public QPrintPreviewDialog
{
public:
    typedef QSharedPointer<QPrinter> PrinterPtr;

    NonModalPreviewDialog(const PrinterPtr &pp, QWidget *p = 0);

    const PrinterPtr m_pp;
};

NonModalPreviewDialog::NonModalPreviewDialog(const PrinterPtr &pp, QWidget *p)
    : QPrintPreviewDialog(pp.data(), p)
    , m_pp(pp)
{
    setModal(false);
    setAttribute(Qt::WA_DeleteOnClose);
    const QSize ds = QApplication::desktop()->availableGeometry(this).size();
    const QSize s = ds * 3/ 4;
    resize(s);
    move((ds.width() - s.width()) / 2, (ds.height() - s.height()) / 2);
    if (p)
    {
        setWindowTitle(p->windowTitle());
    }
}

void ShowInGraphicalShell(const QString &filePath)
{
#ifdef Q_OS_MAC
    QStringList args;
    args << "-e";
    args << "tell application \"Finder\"";
    args << "-e";
    args << "activate";
    args << "-e";
    args << "select POSIX file \""+filePath+"\"";
    args << "-e";
    args << "end tell";
    QProcess::startDetached("osascript", args);
#elif defined(Q_OS_WIN)
    QProcess::startDetached(QString("explorer /select, \"%1\"").arg(QDir::toNativeSeparators(filePath)));
#else
    const QString app = "xdg-open %d";
    QString cmd;
    for (int i = 0; i < app.size(); ++i)
    {
        QChar c = app.at(i);
        if (c == QLatin1Char('%') && i < app.size()-1)
        {
            c = app.at(++i);
            QString s;
            if (c == QLatin1Char('d'))
            {
                s = QLatin1Char('"') + QFileInfo(filePath).path() + QLatin1Char('"');
            }
            else if (c == QLatin1Char('%'))
            {
                s = c;
            }
            else
            {
                s = QLatin1Char('%');
                s += c;
            }
            cmd += s;
            continue;
        }
        cmd += c;
    }
    QProcess::startDetached(cmd);
#endif
}
}

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    InitUI();
}

MainWindow::~MainWindow()
{
    if (!ui->checkBoxKeepPDF->isChecked())
    {
        for (auto &p : m_pdfFiles)
        {
            QFile::remove(p);
        }
    }

    delete ui;
}

QPrinter::PrinterMode MainWindow::printerMode() const
{
    return m_printerMode;
}

void MainWindow::setPrinterMode(const QPrinter::PrinterMode &printerMode)
{
    m_printerMode = printerMode;
}

int MainWindow::pageCount() const
{
    return m_pageCount;
}

void MainWindow::setPageCount(int pageCount)
{
    m_pageCount = pageCount;
}

// Print to printer
void MainWindow::on_actionPrinter_triggered()
{
    if (QPrinterInfo::availablePrinterNames().isEmpty())
    {
        qWarning() << "No printer found";
        return;
    }

    QPrinter printer(m_printerMode);
    QPrintDialog pdlg(&printer);
    if (pdlg.exec() == QDialog::Accepted)
    {
        paintRequested(&printer);
    }
}

void MainWindow::on_actionQuit_triggered()
{
    qApp->quit();
}

void MainWindow::on_actionPrintPDF_triggered()
{
    QPrinter printer(m_printerMode);
    printer.setPageSize(QPrinter::A4);
    printer.setOutputFormat(QPrinter::PdfFormat);
    PrintToFile(printer, "pdf");
}

void MainWindow::on_actionPrintXPS_triggered()
{
    QPrinterInfo xpi;
    for (auto &pi : QPrinterInfo::availablePrinters())
    {
        if (pi.printerName().contains("xps", Qt::CaseInsensitive))
        {
            xpi = pi;
            break;
        }
    }

    if (xpi.isNull())
    {
        qWarning() << "No XPS printer found";
        return;
    }

    QPrinter printer(xpi, m_printerMode);
    qDebug() << "Using" << xpi.printerName();
    printer.setPageSize(QPrinter::A4);
    PrintToFile(printer, "oxps");
}

void MainWindow::on_actionPrintPreview_triggered()
{
    NonModalPreviewDialog::PrinterPtr printer(new QPrinter(m_printerMode));
    printer->setPageSize(QPrinter::A4);
    NonModalPreviewDialog *pd = new NonModalPreviewDialog(printer, this);
    connect(pd, SIGNAL(paintRequested(QPrinter*)), this, SLOT(paintRequested(QPrinter*)));
    pd->show();
}

void MainWindow::on_actionReset_triggered()
{
    for (int row = 0; row < m_pageCount; ++row)
    {
        m_enabled[row]->setChecked(true);
        m_fullPage[row]->setChecked(false);
        m_use53MarginsApi[row]->setChecked(false);
        m_marginsAction[row]->setCurrentIndex(0);
        m_use53MarginsApi[row]->setChecked(true);
        m_leftMargin[row]->setValue(10);
        m_rightMargin[row]->setValue(10);
        m_topMargin[row]->setValue(10);
        m_bottomMargin[row]->setValue(10);
    }
}

void MainWindow::on_actionAboutQt_triggered()
{
    qApp->aboutQt();
}

void MainWindow::on_actionOpenPDFFolder_triggered()
{
    QString folderPath = QDir::tempPath();
    if (!folderPath.endsWith('/'))
    {
        folderPath += '/';
    }

    ShowInGraphicalShell(folderPath);
}

void MainWindow::on_actionAboutApp_triggered()
{
    const QString description = QStringLiteral("QtMargins v%1.\n\nSimple utility to test printing with Qt libary.")
                                    .arg(VERSION);
    QMessageBox::about(this, "About QtMargins", description);
}

void MainWindow::paintRequested(QPrinter *printer)
{
    PainterPtr painter;

    for (int n = 0; n < m_pageCount; ++n)
    {
        if (m_enabled[n]->isChecked())
        {
            const bool fullPage = m_fullPage[n]->isChecked();
            const MarginsAction marginsAction  = (MarginsAction)(m_marginsAction[n]->currentIndex());
            printPageWithFrame(printer, n, fullPage,
                               m_use53MarginsApi[n]->isChecked(),
                               marginsAction, painter);
        }
    }
}

void MainWindow::InitUI()
{
    m_enabled.insert(0, ui->groupBoxPage1);
    m_enabled.insert(1, ui->groupBoxPage2);

    m_fullPage.insert(0, ui->checkBoxPage1Fullpage);
    m_fullPage.insert(1, ui->checkBoxPage2Fullpage);

    m_use53MarginsApi.insert(0, ui->checkBoxPage1Qt53MarginsAPI);
#if QT_VERSION < QT_VERSION_CHECK(5, 3, 0)
    m_use53MarginsApi.at(0)->setEnabled(false);
#endif

    m_use53MarginsApi.insert(1, ui->checkBoxPage2Qt53MarginsAPI);
#if QT_VERSION < QT_VERSION_CHECK(5, 3, 0)
    m_use53MarginsApi.at(1)->setEnabled(false);
#endif

    m_marginsAction.insert(0, ui->comboBoxPage1Margins);
    m_marginsAction.insert(1, ui->comboBoxPage2Margins);

    ui->comboBoxPage1Margins->addItems(marginActionTexts);
    connect(ui->comboBoxPage1Margins, QOverload<int>::of(&QComboBox::currentIndexChanged), [=](int index)
    {
        MarginsAction action = static_cast<MarginsAction>(index);
        ui->groupBoxPage1CustomMargins->setEnabled(action == MarginsAction::MarginsCustom);
    });

    ui->comboBoxPage2Margins->addItems(marginActionTexts);
    connect(ui->comboBoxPage2Margins, QOverload<int>::of(&QComboBox::currentIndexChanged), [=](int index)
    {
        MarginsAction action = static_cast<MarginsAction>(index);
        ui->groupBoxPage2CustomMargins->setEnabled(action == MarginsAction::MarginsCustom);
    });

    m_leftMargin.insert(0, ui->spinBoxPage1LeftMargin);
    m_leftMargin.insert(1, ui->spinBoxPage2LeftMargin);

    m_rightMargin.insert(0, ui->spinBoxPage1RightMargin);
    m_rightMargin.insert(1, ui->spinBoxPage2RightMargin);

    m_topMargin.insert(0, ui->spinBoxPage1TopMargin);
    m_topMargin.insert(1, ui->spinBoxPage2TopMargin);

    m_bottomMargin.insert(0, ui->spinBoxPage1BottomMargin);
    m_bottomMargin.insert(1, ui->spinBoxPage2BottomMargin);

    ui->actionAboutQt->setIcon(style()->standardIcon(QStyle::SP_TitleBarMenuButton));

#if defined(Q_OS_MAC)
    // On Mac deafault icon size is 32x32.
    ui->toolBar->setIconSize(QSize(24, 24));
#endif //defined(Q_OS_MAC)
}

void MainWindow::printPageWithFrame(QPrinter *printer, int page, bool fullPage, bool use53MarginsApi,
                                    MarginsAction marginsAction, PainterPtr &painter)
{
    static int n= 0;
    QString s;
    QDebug debug = QDebug(&s).nospace();
    const bool fullPageChanged = fullPage != printer->fullPage();
    debug << "Qt " << QT_VERSION_STR
          << " platform" << ' '    << QGuiApplication::platformName()
          << " Page #" << (page + 1) << "\nFullPage=" << fullPage;
    if (fullPageChanged)
    {
        debug << "(changed)";
    }
    debug << ", 5.3_API=" << use53MarginsApi
          << ", " << marginActionTexts[marginsAction] << '\n'
          << "Printer name: " << printer->printerName() <<  ' ' << QDate::currentDate().toString()
          << "\nPaint device: " << printer->widthMM() << 'x' << printer->heightMM()
          << "mm DPI logical: " << printer->logicalDpiX() << 'x' << printer->logicalDpiY()
          << " physical: " << printer->physicalDpiX() << 'x' << printer->physicalDpiY()
          <<"\n[Rectangle indicating pageRect]\n";


    // Unless fullpage, 0,0 is at pageRect.topLeft();
    const QRectF origPageRect = printer->pageRect(QPrinter::DevicePixel);
    const QPointF origOffset = fullPage ? QPoint(0, 0) : origPageRect.topLeft();

    dumpPageSize(debug, printer);

    if (fullPageChanged)
    {
        printer->setFullPage(fullPage);
    }

    if (marginsAction != MarginsUnchanged)
    {
        if (use53MarginsApi)
        {
#if QT_VERSION >= QT_VERSION_CHECK(5, 3, 0)
            const QMarginsF oldMargins = printer->pageLayout().margins(QPageLayout::Millimeter);
            QMarginsF newMargins = oldMargins;
            switch (marginsAction)
            {
                case MarginsUnchanged:
                    break;
                case MarginsCleared:
                    newMargins = QMarginsF(0, 0, 0, 0);
                    break;
                case MarginsDoubled:
                    newMargins *= 2;
                    break;
                case MarginsHalved:
                    newMargins /= 2;
                    break;
                case MarginsFixed20:
                    newMargins = QMarginsF(20, 20, 20, 20);
                    break;
                case MarginsFixed40:
                    newMargins = QMarginsF(40, 40, 40, 40);
                    break;
                case MarginsCustom:
                    newMargins = CustomMargins(page);
                    break;
            }
            printer->setPageMargins(newMargins,  QPageLayout::Millimeter);
            debug << "\n QPrinter::setPageMargins(QMarginsF) from " << oldMargins
                  << "\n to " << newMargins << "mm\n";
#else
            qDebug("Unimpl");
#endif
        }
        else
        {
            qreal left, top, right, bottom;
            printer->getPageMargins(&left, &top, &right, &bottom, QPrinter::Millimeter);
            switch (marginsAction)
            {
                case MarginsUnchanged:
                    break;
                case MarginsCleared:
                    left = top = right = bottom = 0;
                    break;
                case MarginsDoubled:
                    left *= 2;
                    top  *= 2;
                    right *= 2;
                    bottom *= 2;
                    break;
                case MarginsHalved:
                    left /= 2;
                    top  /= 2;
                    right /= 2;
                    bottom /= 2;
                    break;
                case MarginsFixed20:
                    left = top = right = bottom = 20;
                    break;
                case MarginsFixed40:
                    left = top = right = bottom = 40;
                    break;
                case MarginsCustom:
                    left = m_leftMargin[page]->value();
                    top = m_topMargin[page]->value();
                    right = m_rightMargin[page]->value();
                    bottom = m_bottomMargin[page]->value();
                    break;
            }
            printer->setPageMargins(left, top, right, bottom, QPrinter::Millimeter);
            debug << "\n QPrinter::setPageMargins(4xqreal): " << left << ", "
                                                              << top << ", "
                                                              << right << ", "
                                                              << bottom << "mm\n";
        }
    }
    if (marginsAction != MarginsUnchanged || fullPageChanged)
    {
        debug <<"\nWith changes applied:\n";
        dumpPageSize(debug, printer);
    }

    if (painter.isNull())
    {
        qDebug() << "CREATING PAINTER";
        painter = PainterPtr(new QPainter(printer));
        QFont f = painter->font();
        f.setPointSize(12);
        f.setFamily("Courier");
        f.setFixedPitch(true);
        painter->setFont(f);
        QPen pen = painter->pen();
        pen.setWidth(2);
        painter->setPen(pen);
    }
    else
    {
        qDebug() << "NEWPAGE";
        printer->newPage();
    }

    painter->save();

    const QRectF  pageRect = printer->pageRect(QPrinter::DevicePixel);
    const QPointF offset = fullPage ? QPoint(0, 0) : pageRect.topLeft();

    // Indicate the old page rect with dashed line if it was inside the new one.
    const QPointF offsetDiff = origOffset  - offset;
    if (offsetDiff.x() > 0 && offsetDiff.y() > 0)
    {
        drawPageRect(origPageRect, origOffset  - offset, *painter.data(), true);
    }
    drawPageRect(pageRect, offset, *painter.data());

    const QFont stdFont = painter->font();
    const int textHeight = QFontMetrics(stdFont).boundingRect("A").height() + 4;

    int y = fullPage ? (pageRect.y() + 60) : 60;
    const int x = fullPage ? (pageRect.x() + 20) : 20;
    for (auto &l: s.split('\n'))
    {
        painter->drawText(x, y, l);
        y += textHeight;
    }

    QFont font = painter->font();
    font.setPointSizeF(18);
    QString largeTextAt00 = "Text at 0,0 " + QString::number(font.pointSize())
                            + "pt,";
    const int largetextHeight = QFontMetrics(font).boundingRect(largeTextAt00).height();
    largeTextAt00 += QString::number(largetextHeight) + "px";
    painter->setFont(font);
    painter->drawText(0, largetextHeight, largeTextAt00);
    painter->setFont(stdFont);

    painter->restore();

    qDebug() << '#' << n++ << __FUNCTION__ << s << '\n';
}

#if QT_VERSION >= QT_VERSION_CHECK(5, 3, 0)
QMarginsF MainWindow::CustomMargins(int page)
{
    return QMarginsF(m_leftMargin[page]->value(),
                     m_topMargin[page]->value(),
                     m_rightMargin[page]->value(),
                     m_bottomMargin[page]->value());
}
#endif

void MainWindow::PrintToFile(QPrinter &printer, const QString &suffix)
{
    QString fileName = QDir::tempPath();
    if (!fileName.endsWith('/'))
    {
        fileName += '/';
    }
    fileName += "qt";
    fileName += QString::fromLatin1(QT_VERSION_STR).remove('.');
    fileName += '_';
    fileName += QGuiApplication::platformName();
    fileName += QString::number(rand() % 1000) + '.' + suffix;
    printer.setOutputFileName(fileName);
    paintRequested(&printer);
    qDebug() << "printed to" << QDir::toNativeSeparators(fileName)
             << QFileInfo(fileName).size();
    if (!ui->checkBoxLaunchPDFViewer->isChecked())
    {
        return;
    }
    if (!QDesktopServices::openUrl(QUrl::fromLocalFile(fileName)))
    {
        qWarning() << suffix << " viewer launch failed";
    }
    m_pdfFiles.append(fileName);
}
