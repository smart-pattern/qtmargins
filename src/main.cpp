/************************************************************************
 **
 **  @file   %{Cpp:License:FileName}
 **  @author Roman Telezhynskyi <dismine(at)gmail.com>
 **  @date   10 3, 2020
 **
 **  Valentina is free software: you can redistribute it and/or modify
 **  it under the terms of the GNU General Public License as published by
 **  the Free Software Foundation, either version 3 of the License, or
 **  (at your option) any later version.
 **
 **  Valentina is distributed in the hope that it will be useful,
 **  but WITHOUT ANY WARRANTY; without even the implied warranty of
 **  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 **  GNU General Public License for more details.
 **
 **  You should have received a copy of the GNU General Public License
 **  along with Valentina.  If not, see <http://www.gnu.org/licenses/>.
 **
 *************************************************************************/
#include "mainwindow.h"

#include <QApplication>
#include <QMessageBox> // For QT_REQUIRE_VERSION
#include <QDebug>
#include <QPrinter>

#include "global.h"

int main(int argc, char *argv[])
{
    QT_REQUIRE_VERSION(argc, argv, "5.0.0")// clazy:exclude=qstring-arg,qstring-allocations

    Q_INIT_RESOURCE(icon);
    Q_INIT_RESOURCE(theme);

#ifndef Q_OS_MAC // supports natively
    InitHighDpiScaling(argc, argv);
#endif //Q_OS_MAC

    QApplication app(argc, argv);

    QStringList args = QCoreApplication::arguments();
    args.pop_front();
    qDebug()<< QT_VERSION_STR << args;

#if !defined(Q_OS_MAC)
    app.setWindowIcon(QIcon("://dist/win/qtmargins.ico"));
#endif // !defined(Q_OS_MAC)

    qDebug()<< QGuiApplication::platformName();

    QPrinter::PrinterMode optPrinterMode = QPrinter::ScreenResolution;
    int optPageCount = 2;

    int index = args.indexOf("-m");
    if (index >= 0)
    {
        optPrinterMode = (QPrinter::PrinterMode)(args.at(index + 1).toInt());
    }

    index = args.indexOf("-p");
    if (index >= 0)
    {
        optPageCount = (QPrinter::PrinterMode)(args.at(index + 1).toInt());
    }
    qDebug() << "mode: " << optPrinterMode << optPageCount << "pages";

    static const char * GENERIC_ICON_TO_CHECK = "document-new";
    if (QIcon::hasThemeIcon(GENERIC_ICON_TO_CHECK) == false)
    {
        //If there is no default working icon theme then we should
        //use an icon theme that we provide via a .qrc file
        //This case happens under Windows and Mac OS X
        //This does not happen under GNOME or KDE
        QIcon::setThemeName(QStringLiteral("fallback_theme"));
    }

    MainWindow w;
    w.setWindowTitle(QLatin1String(QT_VERSION_STR));
    w.setPrinterMode(optPrinterMode);
    w.setPageCount(optPageCount);
    w.show();
    return app.exec();
}
