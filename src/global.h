/************************************************************************
 **
 **  @file   global.h
 **  @author Roman Telezhynskyi <dismine(at)gmail.com>
 **  @date   10 3, 2020.
 **
 **  Valentina is free software: you can redistribute it and/or modify
 **  it under the terms of the GNU General Public License as published by
 **  the Free Software Foundation, either version 3 of the License, or
 **  (at your option) any later version.
 **
 **  Valentina is distributed in the hope that it will be useful,
 **  but WITHOUT ANY WARRANTY; without even the implied warranty of
 **  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 **  GNU General Public License for more details.
 **
 **  You should have received a copy of the GNU General Public License
 **  along with Valentina.  If not, see <http://www.gnu.org/licenses/>.
 **
 *************************************************************************/
#ifndef GLOBAL_H
#define GLOBAL_H

#include <QString>

extern const QString VERSION;

extern const QString LONG_OPTION_NO_HDPI_SCALING;


bool IsOptionSet(int argc, char *argv[], const char *option);
void InitHighDpiScaling(int argc, char *argv[]);

#endif // GLOBAL_H
